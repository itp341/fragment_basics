package tutorial.itp341.com.samplefragment;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by arjunpola on 24/09/15.
 */
public class SampleDialogFragment extends DialogFragment {

    public SampleDialogFragment()
    {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = (View)inflater.inflate(R.layout.dialog_fragment, null, false);
        getDialog().setTitle("Sample Dialog Fragment");
        return view;
    }
}
